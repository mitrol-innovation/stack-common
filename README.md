# Stack Common

## Variables de configuración

### Shared

- `STACK_CODE` *Código de tres dígitos para identificar el stack y asignar puertos*
- `PRODUCTION_RASA_SERVICE_NAME` *Nombre del servicio de producción de rasa, para diferenciarlo del de desarrollo o testing*
- `BOT_WAKE_UP_WORD` *Intención con la que inicia la conversación en un proyecto*
- `NODE_ENV` *Ambiente de NodeJS*


### Analytics
- `ANALYTICS_IMAGE`
- `ANALYTICS_KEY`
- `ANALYTICS_SECRET`
- `ANALYTICS_ADMIN_EMAIL`
- `ANALYTICS_ADMIN_PASSWORD`
- `ANALYTICS_DB_CLIENT`
- `ANALYTICS_DB_DATABASE`
- `ANALYTICS_DB_HOST`
- `ANALYTICS_DB_PORT`
- `ANALYTICS_DB_USER`
- `ANALYTICS_DB_PASSWORD`
- `ANALYTICS_LOG_LEVEL`


### Concordia WebSocket
- `CONCORDIA_WS_IMAGE`
- `CONCORDIA_WS_WAIT_TIME_BETWEEN_MESSAGES` *Tiempo en segundos para esperar entre mensajes antes de condensarlos*
- `CONCORDIA_WS_POLLING_INTERVAL` *Tiempo entre polls en ms*
- `CONCORDIA_WS_MODE` *Modo production o debug*
- `CONCORDIA_WS_LOG_LEVEL` *Nivel de detalle de los logs*
- `CONCORDIA_NODE_ENV` *Ambiente de NodeJS*

### MitACD API
- `MITACD_API_IMAGE`
- `MITACD_API_DB_CLIENT`
- `MITACD_API_DB_HOST`
- `MITACD_API_DB_PORT`
- `MITACD_API_DB_USER`
- `MITACD_API_DB_PASSWORD`
- `MITACD_API_DB_NAME`
- `MITACD_API_LOG_LEVEL`

### Rabbit MQ
- `RABBITMQ_IMAGE`
- `RABBITMQ_HOST` *Ruta de RabbitMQ*


### Voicebot API
- `VOICEBOT_API_IMAGE`
- `VOICEBOT_API_LOG_LEVEL`
- `VOICEBOT_WAKE_UP_WORD`
- `VOICEBOT_NUMBER_CONDENSING_ENABLED` *Booleano para activar condensación de números en la voicebot API*
- `VOICEBOT_NODE_ENV` *Ambiente de NodeJS*

